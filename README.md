# traccar-ansible-playbook
## Easily deploy traccar container on your server

### What this playbook does

It sets up

- [traccar-server](https://hub.docker.com/r/traccar/traccar)
- [postgres database](https://hub.docker.com/_/postgres)
- [exim relay mailer](https://hub.docker.com/r/devture/exim-relay)

and connects them.

### What this playbook does NOT do

It doesn't set up

- a reverse proxy
- ssl

These features are currently in development

### How to use it

1. Create the folder structure "inventory/host_vars/<host-name>/"
2. Place your [hosts-file](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html) in "inventory/host_vars/".
3. Copy "/example/host_vars/host_xy/vars.yml" into "inventory/host_vars/<host-name>/" and modify it to your needs.
4. Optional: Take a look at default variables in roles (roles/<role>/defaults/main.yml). You might want to overwrite some of these in "inventory/host_vars/<host-name>/vars.yml".
5. Run the playbook with `ansible-playbook setup.yml --tags=setup-all,start`.
6. You should re-run the playbook after updates have been made to the docker container versions.

### Special thanks
Special thanks goes to [Slavi](https://github.com/spantaleev/). Much of this playbook, especially the structure, the traccar-mailer and traccar-postgres roles, is his work and the work of all committers of [matrix-docker-ansible-deploy](https://github.com/spantaleev/matrix-docker-ansible-deploy).
